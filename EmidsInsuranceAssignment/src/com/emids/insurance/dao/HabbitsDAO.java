package com.emids.insurance.dao;

import com.emids.insurance.pojos.Habbitspojo;

public interface HabbitsDAO {

	public Habbitspojo getHabbitsById(Long id);
}
