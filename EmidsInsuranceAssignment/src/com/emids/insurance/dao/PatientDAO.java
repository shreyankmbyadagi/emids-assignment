package com.emids.insurance.dao;

import com.emids.insurance.pojos.Patientpojo;

public interface PatientDAO {
	
	public Patientpojo getPatientById(Long id);

}
