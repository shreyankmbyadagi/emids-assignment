package com.emids.insurance.dao;

import com.emids.insurance.pojos.Healthpojo;

public interface HealthDAO {
	
	public Healthpojo getHealthDetailsById(Long id);

}
