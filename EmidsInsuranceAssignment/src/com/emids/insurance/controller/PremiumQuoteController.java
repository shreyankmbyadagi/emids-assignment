package com.emids.insurance.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.emids.insurance.datastorage.HabbitsDAOImpl;
import com.emids.insurance.datastorage.HealthDAOImpl;
import com.emids.insurance.datastorage.PatientDAOImpl;
import com.emids.insurance.pojos.Habbitspojo;
import com.emids.insurance.pojos.Healthpojo;
import com.emids.insurance.pojos.Patientpojo;

@Controller
public class PremiumQuoteController {
	public static ApplicationContext context = new ClassPathXmlApplicationContext("Web.xml");
	
	public static PatientDAOImpl patientdaoimpl = (PatientDAOImpl)context.getBean("patientDAOImpl");
	public static HabbitsDAOImpl habbitsdaoimpl = (HabbitsDAOImpl)context.getBean("habbitsDAOImpl");
	public static HealthDAOImpl healthdaoimpl = (HealthDAOImpl)context.getBean("healthDAOImpl");
	
	@RequestMapping(value="/ui/insurancepremium/patient", method=RequestMethod.GET)
	public ModelAndView getPatientDetails(
			HttpServletRequest request,
			HttpServletResponse response) { 
		
		return new ModelAndView("/WEB-INF/jsp/PatientDataCapture.jsp");
	}
	
	@RequestMapping(value="/ui/insurance/patient/reports/{id}", method=RequestMethod.GET)
	public ModelAndView getPatientPremium(
			@PathVariable(value="id") Long id,
			HttpServletRequest request,
			HttpServletResponse response){
		
		double insurancePremium = 5000L;
		Patientpojo patientpojo = patientdaoimpl.getPatientById(id);
		Healthpojo healthpojo = healthdaoimpl.getHealthDetailsById(patientpojo.getPatientId());
		Habbitspojo habbitspojo = habbitsdaoimpl.getHabbitsById(patientpojo.getPatientId());
		
		if(patientpojo!=null){
			if(patientpojo.getAge()<18){
				request.setAttribute("Insurance Premium", insurancePremium);
			}else{
				if(patientpojo.getAge() >= 18 && patientpojo.getAge() < 40 ){
					insurancePremium =   (insurancePremium + (0.1*5000));
				} else if(patientpojo.getAge() >= 40 ){
					insurancePremium =  (insurancePremium + (0.2*5000));
				}
				if(patientpojo.getGender().equals("male")){
					insurancePremium = (insurancePremium + (0.2*insurancePremium));
				}
				if(healthpojo != null){
					if(healthpojo.getHyperTension() == true)
						insurancePremium =  (insurancePremium + (0.1*insurancePremium));
					if(healthpojo.getBloodPressure() == true) 
						insurancePremium =  (insurancePremium + (0.1*insurancePremium));
					if(healthpojo.getBloodSugar() == true)
						insurancePremium =  (insurancePremium + (0.1*insurancePremium));
					
					if(healthpojo.getOverWeight() == true)
						insurancePremium =  (insurancePremium + (0.1*insurancePremium));		
				}
				if(habbitspojo != null){
					if(habbitspojo.getSmoking() == true)
						insurancePremium =  (insurancePremium + (0.3*insurancePremium));
					if(habbitspojo.getAlcohol() == true)
						insurancePremium =  (insurancePremium + (0.3*insurancePremium));
					if(habbitspojo.getDailyExercise() == true)
						insurancePremium =  (insurancePremium - (0.3*insurancePremium));
							
					if(habbitspojo.getDrugs() == true)
						insurancePremium =  (insurancePremium + (0.3*insurancePremium));
				}
				request.setAttribute("Insurance premium", insurancePremium);
			}
		}
		return new ModelAndView("/WEB-INF/jsp/PremiumQuote.jsp");
	}
	
	
}
