package com.emids.insurance.pojos;

public class Healthpojo {

	private Long id;
	private Long patientId;
	private Boolean bloodPressure;
	private Boolean bloodSugar;
    private Boolean hyperTension;
	private Boolean overWeight;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getPatientId() {
		return patientId;
	}
	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}
	public Boolean getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(Boolean bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public Boolean getBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(Boolean bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public Boolean getHyperTension() {
		return hyperTension;
	}
	public void setHyperTension(Boolean hyperTension) {
		this.hyperTension = hyperTension;
	}
	public Boolean getOverWeight() {
		return overWeight;
	}
	public void setOverWeight(Boolean overWeight) {
		this.overWeight = overWeight;
	}
		
	
	
}
