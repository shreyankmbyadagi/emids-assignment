package com.emids.insurance.pojos;

public class Habbitspojo {
	
	private Long id;
	private Long patientId;
	private Boolean alcohol;
	private Boolean dailyExercise;
	private Boolean drugs;
	private Boolean smoking;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getPatientId() {
		return patientId;
	}
	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}
	public Boolean getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(Boolean alcohol) {
		this.alcohol = alcohol;
	}
	public Boolean getDailyExercise() {
		return dailyExercise;
	}
	public void setDailyExercise(Boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}
	public Boolean getDrugs() {
		return drugs;
	}
	public void setDrugs(Boolean drugs) {
		this.drugs = drugs;
	}
	public Boolean getSmoking() {
		return smoking;
	}
	public void setSmoking(Boolean smoking) {
		this.smoking = smoking;
	}
	
	
	

}
