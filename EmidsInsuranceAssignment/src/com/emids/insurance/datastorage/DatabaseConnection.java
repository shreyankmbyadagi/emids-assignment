package com.emids.insurance.datastorage;

import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseConnection {
	
	public static Connection getConnection()
	{
		Connection con=null;
		try{  
			Class.forName("com.mysql.jdbc.Driver");  
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/emids","root","password");  
			
		} catch(Exception e)
		{
			System.out.println(e);
		}
		return con;
	}

}
