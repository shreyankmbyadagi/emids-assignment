package com.emids.insurance.datastorage;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.emids.insurance.dao.HealthDAO;
import com.emids.insurance.pojos.Healthpojo;



public class HealthDAOImpl implements HealthDAO{
	
	private DataSource dataSource;
	   private JdbcTemplate jdbcTemplateObject;
	   
	   public void setDataSource(DataSource dataSource) {
	      this.dataSource = dataSource;
	      this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	   }

	public Healthpojo getHealthDetailsById(Long id){
		String SQL = "select * from patienthealth where patientid = ?";
		Healthpojo healthpojo = jdbcTemplateObject.queryForObject(SQL, 
	         new Object[]{id}, new HealthDataMapper());
	      
	      return healthpojo;
	}
	
	public class HealthDataMapper implements RowMapper<Healthpojo> {
		   public Healthpojo mapRow(ResultSet rs, int rowNum) throws SQLException {
			   Healthpojo healthpojo = new Healthpojo();
			   healthpojo.setId(rs.getLong("id"));
			   healthpojo.setPatientId(rs.getLong("patientId"));
		      
		      return healthpojo;
		   }
		}
}
