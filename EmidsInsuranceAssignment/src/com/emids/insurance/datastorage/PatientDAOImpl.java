package com.emids.insurance.datastorage;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.emids.insurance.dao.PatientDAO;
import com.emids.insurance.pojos.Patientpojo;



public class PatientDAOImpl implements PatientDAO  {
	
	   private DataSource dataSource;
	   private JdbcTemplate jdbcTemplateObject;
	   
	   public void setDataSource(DataSource dataSource) {
	      this.dataSource = dataSource;
	      this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	   }
	
	public Patientpojo getPatientById(Long id) {
		String SQL = "select * from patient_tbl where id = ?";
		Patientpojo patientpojo = jdbcTemplateObject.queryForObject(SQL, 
		         new Object[]{id}, new PatientDataMapper());
		return patientpojo;
	}
	
	public class PatientDataMapper implements RowMapper<Patientpojo> {
		   public Patientpojo mapRow(ResultSet rs, int rowNum) throws SQLException {
			   Patientpojo patientpojo = new Patientpojo();
			   patientpojo.setId(rs.getLong("id"));
			   patientpojo.setFirstName(rs.getString("firstname"));
			   patientpojo.setAge(rs.getInt("age"));
		      
		      return patientpojo;
		   }
		}

}
