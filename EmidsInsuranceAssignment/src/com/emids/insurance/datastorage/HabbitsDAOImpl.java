package com.emids.insurance.datastorage;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.emids.insurance.dao.HabbitsDAO;
import com.emids.insurance.pojos.Habbitspojo;



public class HabbitsDAOImpl implements HabbitsDAO {
	
	private DataSource dataSource;
	   private JdbcTemplate jdbcTemplateObject;
	   
	   public void setDataSource(DataSource dataSource) {
	      this.dataSource = dataSource;
	      this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	   }

	public Habbitspojo getHabbitsById(Long id){
		String SQL = "select * from habits_tbl where patientid = ?"; 
		Habbitspojo habbitspojo = jdbcTemplateObject.queryForObject(SQL, 
		         new Object[]{id}, new HabitsDataMapper());
	      
	      return habbitspojo;
	}
	
	public class HabitsDataMapper implements RowMapper<Habbitspojo> {
		   public Habbitspojo mapRow(ResultSet rs, int rowNum) throws SQLException {
			   Habbitspojo habbitspojo = new Habbitspojo();
			   habbitspojo.setId(rs.getLong("id"));
			   habbitspojo.setPatientId(rs.getLong("patientId"));
		      
		      return habbitspojo;
		   }
		}
}
